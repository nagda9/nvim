local options = {
  autoindent = true,                              -- Apply the indentation of the current line to the next
  autowriteall = true,                            -- Enable autosave
  backup = false,                                 -- Disable backup file creation
  breakindent = true,                             -- Enable break indent
  clipboard = "unnamedplus",                      -- Allows Neovim to access the system clipboard
  cmdheight = 1,                                  -- Space in the command line for displaying messages
  completeopt = { "menuone", "noselect" },        -- Mostly for nvim-cmp
  conceallevel = 1,                               -- So that `` is visible in markdown files
  cursorline = true,                              -- Highlight the current line
  expandtab = true,                               -- Convert tabs to spaces
  fileencoding = "utf-8",                         -- Encoding written to a file
  foldcolumn = "1",                               -- Controls the width of the fold indicator column
  foldenable = true,                              -- Enable folding
  foldlevel = 99,                                 -- Levels of folding visible while editing
  foldlevelstart = 99,                            -- Levels of folding visible when opening a file
  foldmethod = "manual",                          -- Keep it manual, the other options are bad
  guifont = "monospace:h17",                      -- Font used in graphical Neovim applications
  hlsearch = true,                                -- Highlight all matches on the previous search pattern
  ignorecase = true,                              -- Ignore case in search patterns
  incsearch = true,                               -- Incremental search
  iskeyword = vim.opt.iskeyword:append("-"),      -- Recognize hyphenated words by searches
  linebreak = true,                               -- Avoid splitting words when wrapping
  mouse = "a",                                    -- Enable mouse support
  number = true,                                  -- Show absolute line numbers
  numberwidth = 4,                                -- Number column width
  pumheight = 10,                                 -- Popup menu height
  relativenumber = false,                         -- Disable relative line numbers
  scrolloff = 8,                                  -- Minimum screen lines above/below the cursor
  shiftwidth = 2,                                 -- Spaces per indentation
  sessionoptions = vim.opt.sessionoptions:append("folds"),
  sidescrolloff = 2,                              -- Minimum screen columns around the cursor
  signcolumn = "yes:2",                           -- Always show sign column
  smartcase = true,                               -- Use smart case matching
  smartindent = false,                            -- Do not react to code syntax/style for indentation
  splitbelow = true,                              -- Horizontal splits go below the current window
  splitright = true,                              -- Vertical splits go to the right of the current window
  swapfile = false,                               -- Disable swap file creation
  tabstop = 2,                                    -- Insert 2 spaces for a tab
  termguicolors = true,                           -- Enable 24-bit color support
  textwidth = 69,                                 -- Maximum width of text for wrapping
  timeout = true,                                 -- Enable mapped sequence timeouts
  timeoutlen = 300,                               -- Mapped sequence wait time
  undodir = os.getenv("HOME") .. "/.vim/undodir", -- Undo file directory
  undofile = true,                                -- Enable persistent undo
  updatetime = 250,                               -- Faster completion
  whichwrap = "bs<>[]hl",                         -- Keys allowed to travel between lines
  wrap = false,                                   -- Disable text wrapping
  writebackup = false,                            -- Disable write backup files
}

for k, v in pairs(options) do
  vim.opt[k] = v
end

-- Remap space as leader key
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Disable netrw
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1
