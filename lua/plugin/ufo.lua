return {
  "kevinhwang91/nvim-ufo",
  dependencies = { 'kevinhwang91/promise-async' },

  config = function()
    local dashes = function()
      local width = vim.fn.winwidth(0) -- Get the width of the current window
      return string.rep("-", width)
    end

    local handler = function(virtText, lnum, endLnum, width, truncate)
      local newVirtText = {}
      local suffix = (" 󰇘 Folded %d lines "):format(endLnum - lnum)
      local sufWidth = vim.fn.strdisplaywidth(suffix)
      local targetWidth = width - sufWidth
      local curWidth = 0
      for _, chunk in ipairs(virtText) do
        local chunkText = chunk[1]
        local chunkWidth = vim.fn.strdisplaywidth(chunkText)
        if targetWidth > curWidth + chunkWidth then
          table.insert(newVirtText, chunk)
        else
          chunkText = truncate(chunkText, targetWidth - curWidth)
          local hlGroup = chunk[2]
          table.insert(newVirtText, { chunkText, hlGroup })
          chunkWidth = vim.fn.strdisplaywidth(chunkText)
          -- str width returned from truncate() may less than 2nd argument, need padding
          if curWidth + chunkWidth < targetWidth then
            suffix = suffix .. (" "):rep(targetWidth - curWidth - chunkWidth)
          end
          break
        end
        curWidth = curWidth + chunkWidth
      end
      table.insert(newVirtText, { suffix, "MoreMsg" })
      table.insert(newVirtText, { dashes(), "MoreMsg" })
      return newVirtText
    end

    local ufo = require("ufo")

    ufo.setup({
      fold_virt_text_handler = handler,
      provider_selector = function(bufnr, filetype, buftype)
        -- Note: two choices:
        -- 1) Use providers: If foldmethod option is not 'diff' or
        --    'marker', it will request the providers to get the
        --    folds. Main providers: 'lsp', 'treesitter'. The
        --    returned value is a dict of 2 values: a main provider
        --    and a fallback option.
        return { "lsp", "indent" }
        -- 2) Dont use provider, which means you have the default
        --    foldmethod options
        -- return ''
      end,
    })

    -- [R]eveal all folds
    vim.keymap.set("n", "zR", ufo.openAllFolds)
    -- [M]ute all folds
    vim.keymap.set("n", "zM", ufo.closeAllFolds)
    -- -- pea[K] - just toggle the fold instead with za
    -- vim.keymap.set("n", "zK", function()
    --   local winid = ufo.peekFoldedLinesUnderCursor()
    --   if not winid then
    --     vim.lsp.buf.hover()
    --   end
    -- end)

    vim.api.nvim_set_hl(0, 'Folded', { link = 'Normal' })  -- Match folded lines with normal background
    vim.api.nvim_set_hl(0, 'UfoFoldedBg', { bg = 'NONE' }) -- Remove background highlight for ufo.nvim folds
  end
}
