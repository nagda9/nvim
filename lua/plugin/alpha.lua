-- Alpha
return {
  "goolord/alpha-nvim",
  lazy = false,
  config = function()
    local alpha = require("alpha")
    local dashboard = require("alpha.themes.dashboard")
    dashboard.section.header.val = {
      [[                                                    ]],
      [[                                                    ]],
      [[                                                    ]],
      [[                                                    ]],
      [[                                                    ]],
      [[                                                    ]],
      [[                                                    ]],
      [[                                                    ]],
      [[                                                    ]],
      [[                                                    ]],
      [[                                                    ]],
      [[ ███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗ ]],
      [[ ████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║ ]],
      [[ ██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║ ]],
      [[ ██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║ ]],
      [[ ██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║ ]],
      [[ ╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝ ]],
    }
    dashboard.section.footer.val = {
      [[           Welcome to the          ]],
      [[                          _        ]],
      [[                _        (_)       ]],
      [[  ____  _____ _| |_  ____ _ _   _  ]],
      [[ |    \(____ (_   _)/ ___) ( \ / ) ]],
      [[ | | | / ___ | | |_| |   | |) X (  ]],
      [[ |_|_|_\_____|  \__)_|   |_(_/ \_) ]],
    }
    dashboard.section.buttons.val = {
      -- Create a dummy button
      {
        type = "button",
        val = "",
        on_press = function()
        end,
        opts = {
          position = "center",
          cursor = 0,
          width = 1,
        }
      },
    }
    alpha.setup(dashboard.opts)

    -- Bring up alpha when all buffers are closed
    vim.api.nvim_create_augroup("AlphaOnEmpty", { clear = true })

    local AlphaState = false
    vim.api.nvim_create_autocmd("User", {
      group = "AlphaOnEmpty",
      desc = "Change local AlphaState variable to true when it is loaded.",
      pattern = "AlphaReady",
      callback = function()
        AlphaState = true
      end
    })
    vim.api.nvim_create_autocmd("User", {
      group = "AlphaOnEmpty",
      desc = "Change local AlphaState variable to false when it is unloaded.",
      pattern = "AlphaClosed",
      callback = function()
        AlphaState = false
      end
    })

    vim.api.nvim_create_autocmd("BufEnter", {
      group = "AlphaOnEmpty",
      desc = "Open Alpha if no open buffers left.",
      pattern = "*",
      callback = function()
        vim.defer_fn(function()
          local bufnr = vim.api.nvim_get_current_buf()
          local name = vim.api.nvim_buf_get_name(bufnr)
          local buftype = vim.api.nvim_buf_get_option(bufnr, 'buftype')

          local win_id = vim.api.nvim_get_current_win()
          local win_info = vim.fn.getwininfo(win_id)
          local is_quickfix = win_info[1].quickfix == 1
          local is_locklist = win_info[1].locklist == 1

          if not is_quickfix and not is_locklist and buftype ~= "nofile" and buftype ~= "prompt" and name == "" and AlphaState == false then
            AlphaState = true
            vim.cmd(":Alpha | bd #")
          end
        end, 100)
      end,
    })
  end
}
