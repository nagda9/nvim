return {
  "svban/YankAssassin.nvim",
  config = function()
    require("YankAssassin").setup {
      auto_normal = true, -- Whenever y is used in normal mode, the cursor doesn't move to start
      auto_visual = true, -- Whenever y is used in visual mode, the cursor doesn't move to start
    }
  end,
}
