return {
  "danymat/neogen",
  config = function()
    require('neogen').setup({
      snippet_engine = "luasnip",
    })

    vim.api.nvim_set_keymap("n", "<Leader>ng", ":lua require('neogen').generate()<CR>", { noremap = true, silent = true })
  end
}
