-- All plugin comes here that I don't need any config
return {
  -- Properly close buffers, BDeletePre and BDeletePost events
  { "famiu/bufdelete.nvim" },

  -- Toggle relative numbers around current line
  { "sitiom/nvim-numbertoggle" },

  -- Commenting
  {
    "numToStr/Comment.nvim",
    lazy = false,
    opts = {},
  },

  -- Scrollbar with diagnostics
  {
    "petertriho/nvim-scrollbar",
    opts = {},
  },

  -- Git
  { "https://github.com/tpope/vim-fugitive" },
}
