return {
  "mbbill/undotree",
  config = function()
    vim.api.nvim_set_var("undotree_SetFocusWhenToggle", 1)
    vim.api.nvim_set_var("undotree_WindowLayout", 4)
  end
}
