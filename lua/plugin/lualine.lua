return {
  -- Statusline and tabline
  "nvim-lualine/lualine.nvim",
  lazy = false,
  dependencies = {
    { "linrongbin16/lsp-progress.nvim" }, -- Progress indicators
  },

  config = function()
    require("lsp-progress").setup({
      client_format = function(client_name, spinner, series_messages)
        if #series_messages == 0 then
          return nil
        end
        return {
          name = client_name,
          body = spinner .. " " .. table.concat(series_messages, ", "),
        }
      end,

      format = function(client_messages)
        local function stringify(name, msg)
          return msg and string.format("%s %s", name, msg) or name
        end

        local sign = ""
        local lsp_clients = vim.lsp.get_active_clients()
        local messages_map = {}
        for _, climsg in ipairs(client_messages) do
          messages_map[climsg.name] = climsg.body
        end

        if #lsp_clients > 0 then
          table.sort(lsp_clients, function(a, b)
            return a.name < b.name
          end)
          local builder = {}
          for _, cli in ipairs(lsp_clients) do
            if type(cli) == "table" and type(cli.name) == "string" and string.len(cli.name) > 0 then
              if messages_map[cli.name] then
                table.insert(builder, stringify(cli.name, messages_map[cli.name]))
              else
                table.insert(builder, stringify(cli.name))
              end
            end
          end
          if #builder > 0 then
            return sign .. " " .. table.concat(builder, ", ")
          end
        end
        return ""
      end,
    })

    local diagnostics = {
      "diagnostics",
      sources = { "nvim_diagnostic" },
      sections = { "error", "warn" },
      symbols = { error = " ", warn = " " },
      update_in_insert = false,
      always_visible = false,
      colored = true,
    }

    local mode = {
      "mode",
      fmt = function(str)
        return str
      end,
    }

    local branch = {
      "branch",
      icons_enabled = true,
      icon = "",
    }

    local spaces = {
      function()
        return "spaces: " .. vim.api.nvim_buf_get_option(0, "shiftwidth")
      end,
    }

    local cwd = {
      function()
        return "" .. vim.loop.cwd()
      end,
    }

    local shortened_cwd = {
      function()
        local cwd = vim.fn.getcwd()
        local max_length = 30 -- Set your desired maximum length
        if #cwd > max_length then
          local truncated = "..." .. cwd:sub(-max_length)
          local slash_pos = truncated:find("/")
          if slash_pos then
            return "..." .. truncated:sub(slash_pos)
          end
          return truncated
        end
        return cwd
      end
    }

    local lsp = {
      function()
        return require('lsp-progress').progress()
      end
    }

    require("lualine").setup({
      options = {
        icons_enabled = true,
        theme = "auto",
        component_separators = { left = "", right = "" },
        section_separators = { left = "", right = "" },
        disabled_filetypes = { "dashboard", "NvimTree", "Outline" }, -- add "alpha" if you dont want statusline on alpha
        always_divide_middle = true,
      },
      sections = {
        lualine_a = { mode },
        lualine_b = { shortened_cwd },
        lualine_c = {
          branch,
          diagnostics,
          lsp,
        },
        lualine_x = { spaces, "encoding", "filetype" },
        lualine_y = {},
        lualine_z = { "location" },
      },
      inactive_sections = {
        lualine_a = {},
        lualine_b = {},
        lualine_c = {},
        lualine_x = {},
        lualine_y = {},
        lualine_z = {},
      },
      tabline = {
        lualine_a = { "buffers" },
        lualine_b = {},
        lualine_c = {},
        lualine_x = {},
        lualine_y = {},
        lualine_z = { "tabs" },
      },
      extensions = {},
    })
  end
}
