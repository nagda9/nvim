return {
  "stevearc/oil.nvim",
  event = "VeryLazy",
  config = function()
    require("oil").setup({
      keymaps = {
        ["<Esc>"] = "actions.close",
      },
      view_options = {
        -- Show files and directories that start with "."
        show_hidden = false, -- but you can toggle hidden files with 'g.'
      }
    })
  end
}
