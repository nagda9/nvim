return {
  -- LSP (Language Server Protocol)
  -- Language features like auto complete, go to definition, find all references etc.
  "neovim/nvim-lspconfig",
  dependencies = {
    -- Mason - LSP package manager
    -- Easily install and manage LSP servers, DAP servers, linters, and formatters.
    "williamboman/mason.nvim",

    -- Mason LSP config
    -- Bridges lspconfig and mason.
    "williamboman/mason-lspconfig.nvim",

    -- Completition
    'hrsh7th/cmp-nvim-lsp',                  -- LSP based completitions
    "hrsh7th/cmp-buffer",                    -- cmp options based on words in the file
    "hrsh7th/cmp-path",                      -- path options
    "hrsh7th/cmp-cmdline",                   -- command line options
    "hrsh7th/cmp-calc",                      -- calculation result as completition option
    "hrsh7th/cmp-emoji",                     -- insert emojis
    "hrsh7th/cmp-nvim-lua",                  -- LSP like cmp for vim commands
    "hrsh7th/nvim-cmp",
    "chrisgrieser/cmp-nerdfont",             -- insert nerdfont characters
    "kdheepak/cmp-latex-symbols",            -- insert latex symbols
    "davidsierradz/cmp-conventionalcommits", -- help generate conventional commits: https://www.conventionalcommits.org/en/v1.0.0/#specification
    "petertriho/cmp-git",                    -- list git completitions
    {
      "Dynge/gitmoji.nvim",                  -- list gitmojis
      ft = "gitcommit",
    },

    -- Snippets
    "L3MON4D3/LuaSnip",
    "saadparwaiz1/cmp_luasnip",
    {
      "madskjeldgaard/cheeky-snippets.nvim",
      dependencies = {
        "L3MON4D3/LuaSnip"
      },
      opts = {},
    },

    -- Autopairs
    {
      'windwp/nvim-autopairs',
      event = "InsertEnter",
    },

    -- -- Signatures
    {
      "ray-x/lsp_signature.nvim",
      event = "VeryLazy",
      config = function()
        require("lsp_signature").setup({
          bind = true, -- This is mandatory, otherwise border config won't get registered.
          handler_opts = {
            border = "rounded",
          },
          hint_enable = false,
        })
      end
    }
  },

  config = function()
    --[ CMP setup ]--
    local kind_icons = {
      Text = "󰉿",
      Method = "󰆧",
      Function = "󰊕",
      Constructor = "",
      Field = " ",
      Variable = "󰀫",
      Class = "󰠱",
      Interface = "",
      Module = "",
      Property = "󰜢",
      Unit = "󰑭",
      Value = "󰎠",
      Enum = "",
      Keyword = "󰌋",
      Snippet = "",
      Color = "󰏘",
      File = "󰈙",
      Reference = "",
      Folder = "󰉋",
      EnumMember = "",
      Constant = "󰏿",
      Struct = "",
      Event = "",
      Operator = "󰆕",
      TypeParameter = " ",
      Misc = " ",
      Copilot = "",
    }

    local cmp = require("cmp")
    local luasnip = require("luasnip")
    cmp.setup({
      -- snippet REQUIRED - you must specify snippet engine
      snippet = {
        expand = function(args)
          luasnip.lsp_expand(args.body)
        end,
      },

      window = {
        completion = cmp.config.window.bordered(),
        documentation = cmp.config.window.bordered(),
      },

      mapping = {
        -- It is possible to specify the modes the mapping should be active:
        -- `i` = insert mode
        -- `c` = command mode
        -- `s` = select mode

        ["<C-c>"] = cmp.mapping.abort(),

        -- Activate the completition menu
        ["<C-Space>"] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }),

        ['<Tab>'] = cmp.mapping(function(fallback)
          if cmp.visible() then
            cmp.select_next_item({ behavior = cmp.SelectBehavior.Insert })
          elseif luasnip.expand_or_jumpable() then
            luasnip.expand_or_jump()
          else
            fallback()
          end
        end, { 'i', 's' }),

        ['<S-Tab>'] = cmp.mapping(function(fallback)
          if cmp.visible() then
            cmp.select_prev_item({ behavior = cmp.SelectBehavior.Insert })
          elseif luasnip.jumpable(-1) then
            luasnip.jump(-1)
          else
            fallback()
          end
        end, { 'i', 's' }),

        -- -- Note: <C-Tab> does not work with tmux, thats why <C-n>
        -- ["<C-n>"] = cmp.mapping(function(fallback)
        --   if luasnip.expand_or_jumpable() then
        --     luasnip.expand_or_jump()
        --   else
        --     fallback()
        --   end
        -- end, { "i", "n", "s" }),
        --
        -- ["<C-S-n>"] = cmp.mapping(function(fallback)
        --   if luasnip.jumpable(-1) then
        --     luasnip.jump(-1)
        --   else
        --     fallback()
        --   end
        -- end, { "i", "n", "s" }),

        ["<CR>"] = cmp.mapping.confirm({
          behavior = cmp.ConfirmBehavior.Replace,
        }),

        -- Close completition on down arrow
        ["<Down>"] = cmp.mapping(function(fallback)
          if cmp.visible() then
            cmp.close()
          end
          fallback()
        end, { "i", "c" }),

        -- Close completition on up arrow
        ["<Up>"] = cmp.mapping(function(fallback)
          if cmp.visible() then
            cmp.close()
          end
          fallback()
        end, { "i", "c" }),
      },

      -- Completition sources - Order matters!
      -- List of sources:
      -- https://github.com/hrsh7th/nvim-cmp/wiki/List-of-sources
      sources = {
        { name = "nvim_lsp" }, -- complete code with lsp ("hrsh7th/cmp-nvim-lsp")
        { name = "nvim_lua" }, -- works like an LSP for vim
        { name = "luasnip" },  -- code snippets ("saadparwaiz1/cmp_luasnip")
        { name = "buffer" },   -- complete words based on words from the file ("hrsh7th/cmp-buffer")
        { name = "path" },     -- complete paths ("hrsh7th/cmp-path")
        { name = "calc" },     -- show calculation results
        { name = "nerdfont" },
        { name = "emoji" },

        -- Strategy options:
        -- 0 - mixed Show the command and insert the symbol
        -- 1 - julia Show and insert the symbol
        -- 2 - latex Show and insert the command
        { name = "latex_symbols", option = { strategy = 0 } },
      },

      -- how the completition menu looks
      formatting = {
        fields = { "kind", "abbr", "menu" }, -- kind symbol, abbreviation, source
        format = function(entry, vim_item)
          -- Kind icons
          vim_item.kind = string.format("%s", kind_icons[vim_item.kind])
          vim_item.menu = ({
            nvim_lsp = "[LSP]",
            nvim_lua = "[Lua]",
            luasnip = "[Snippet]",
            buffer = "[Buffer]",
            path = "[Path]",
            calc = "[Calc]",
            latex_symbols = "[Latex]",
            nerdfont = "[Nerd]",
            emoji = "[Emoji]",
            gitmoji = "[Gitmoji]",
            git = "[Git]",
            conventionalcommits = "[CC]",
          })[entry.source.name]
          return vim_item
        end,
      },
    })

    -- Set configuration for specific filetype.
    cmp.setup.filetype("gitcommit", {
      sources = {
        { name = "conventionalcommits" }, -- conventional commits ("davidsierradz/cmp-conventionalcommits")
        { name = "git" },                 -- completitions for git ("petertriho/cmp-git")
        { name = "buffer" },
        { name = "gitmoji" },             -- git emojis
      },
    })

    -- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
    cmp.setup.cmdline({ "/", "?" }, {
      mapping = cmp.mapping.preset.cmdline(),
      sources = {
        { name = "buffer" },
      },
    })

    -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
    cmp.setup.cmdline(":", {
      mapping = cmp.mapping.preset.cmdline(),
      sources = {
        -- { name = "path" },
        { name = "path" },
        { name = "cmdline" }, -- completitions for cmdline ("hrsh7th/cmp-cmdline")
      },
    })

    --[ Autopairs ]--
    require('nvim-autopairs').setup({
      -- Fast wrap behavior:
      -- Before        Input                    After         Note
      -- -----------------------------------------------------------------
      -- (|foobar      <M-e> then press $       (|foobar)
      -- (|)(foobar)   <M-e> then press q       (|(foobar))
      -- (|foo bar     <M-e> then press qh      (|foo) bar
      -- (|foo bar     <M-e> then press qH      (foo|) bar
      -- (|foo bar     <M-e> then press qH      (foo)| bar    if cursor_pos_before = false
      fast_wrap = {},
    })
    local cmp_autopairs = require('nvim-autopairs.completion.cmp')

    -- Add () automatically after completion
    cmp.event:on('confirm_done', cmp_autopairs.on_confirm_done())

    --[ LSP setup ]--
    local cmp_lsp = require("cmp_nvim_lsp")

    local lsp_capabilities = vim.lsp.protocol.make_client_capabilities()

    -- Tell the server the capability of foldingRange,
    -- Neovim hasn't added foldingRange to default capabilities, we have to add it manually
    lsp_capabilities.textDocument.foldingRange = {
      dynamicRegistration = false,
      lineFoldingOnly = true
    }

    local capabilities = vim.tbl_deep_extend(
      "force",
      {},
      lsp_capabilities,
      cmp_lsp.default_capabilities()
    )

    require("mason").setup({})
    require("mason-lspconfig").setup({
      ensure_installed = {
        "lua_ls",
        "cmake",
      },
      automatic_installation = true,
      handlers = {
        -- This is the default handler.
        -- Called for each server that is not overriden below.
        function(server)
          require("lspconfig")[server].setup({
            capabilities = capabilities,
            flags = {
              debounce_text_changes = 50, -- Update diagnostics more often
            },
            settings = {
              documentSync = {
                change = vim.lsp.protocol.TextDocumentSyncKind.Incremental,
              },
            },
          })
        end,

        -- Custom configs for individual lsp servers
        ["lua_ls"] = function()
          require("lspconfig").lua_ls.setup({
            capabilities = capabilities,
            settings = {
              Lua = {
                diagnostics = {
                  globals = { "vim", "bit", "it", "describe", "before_each", "after_each" },
                },
                workspace = {
                  library = {
                    vim.env.VIMRUNTIME,
                  },
                },
              },
            },
          })
        end,
      },
    })

    --[ Register buffer-specific LSP functionalities ]--
    -- They only work if you have an active language server for the buffer
    local autocmd = vim.api.nvim_create_autocmd
    local augroup = vim.api.nvim_create_augroup

    local lspconfig_group = augroup("LspConfig", { clear = true })
    autocmd("LspAttach", {
      group = lspconfig_group,
      desc = "Register buffer local LSP functionalities.",
      callback = function(event)
        local buffer = event.buf
        local client = vim.lsp.get_client_by_id(event.data.client_id)

        -- Format before save
        -- Only create this autocmd after LSP was attached
        if client.supports_method("textDocument/formatting") then
          local formatting_group = augroup("LspFormatting", { clear = true })
          autocmd("BufWritePre", {
            group = formatting_group,
            desc = "Format using LSP before save.",
            buffer = buffer,
            callback = function()
              vim.lsp.buf.format()
            end,
          })
        end

        local diagnostics_group = augroup("LspDiagnostics", { clear = true })
        -- Hide diagnostics on entering insert mode
        autocmd("InsertEnter", {
          group = diagnostics_group,
          desc = "Hide diagnostics when entering mode.",
          buffer = buffer,
          callback = function()
            vim.diagnostic.hide() -- Hide diagnostics display when entering insert mode
          end,
        })
        -- Show and update diagnostics on leaving insert mode
        autocmd("InsertLeave", {
          group = diagnostics_group,
          desc = "Update and show diagnostics after leaving insert mode.",
          buffer = buffer,
          callback = function()
            vim.diagnostic.show()            -- Show diagnostics display after leaving insert mode
            vim.lsp.buf.document_highlight() -- Force LSP to refresh
            vim.diagnostic.setloclist({ open = false })
          end,
        })

        -- Set LSP keymaps
        local keymap = vim.api.nvim_buf_set_keymap
        keymap(
          buffer,
          "n",
          "K",
          ":lua vim.lsp.buf.hover()<cr>",
          { noremap = true, silent = true, desc = "Hover" }
        )
        keymap(
          buffer,
          "n",
          "gd",
          ":lua vim.lsp.buf.definition()<cr>",
          { noremap = true, silent = true, desc = "Go to definition" }
        )
        keymap(
          buffer,
          "n",
          "gD",
          ":lua vim.lsp.buf.declaration()<cr>",
          { noremap = true, silent = true, desc = "Go to declaration" }
        )
        keymap(
          buffer,
          "n",
          "gi",
          ":lua vim.lsp.buf.implementation()<cr>",
          { noremap = true, silent = true, desc = "Go to implementation" }
        )
        keymap(
          buffer,
          "n",
          "go",
          ":lua vim.lsp.buf.type_definition()<cr>",
          { noremap = true, silent = true, desc = "Go to type definition" }
        )
        keymap(
          buffer,
          "n",
          "gr",
          ":lua vim.lsp.buf.references()<cr>",
          { noremap = true, silent = true, desc = "Go to references" }
        )
        keymap(
          buffer,
          "n",
          "<leader>lf",
          ":lua vim.lsp.buf.format{ async = true }<cr>",
          { noremap = true, silent = true, desc = "LSP format" }
        )
        keymap(
          buffer,
          "n",
          "<leader>li",
          ":LspInfo<cr>",
          { noremap = true, silent = true, desc = "LSP info" }
        )
        keymap(
          buffer,
          "n",
          "<leader>la",
          ":lua vim.lsp.buf.code_action()<cr>",
          { noremap = true, silent = true, desc = "LSP code actions" }
        )
        keymap(
          buffer,
          "n",
          "<leader>lr",
          ":lua vim.lsp.buf.rename()<cr>",
          { noremap = true, silent = true, desc = "Rename variable" }
        )
        keymap(
          buffer,
          "n",
          "]l",
          ":lua vim.diagnostic.goto_next()<cr>",
          { noremap = true, silent = true, desc = "Jump to next diagnostic" }
        )
        keymap(
          buffer,
          "n",
          "[l",
          ":lua vim.diagnostic.goto_prev()<cr>",
          { noremap = true, silent = true, desc = "Jump to prev diagnostic" }
        )
      end,
    })
  end
}
