return {
  "nvim-treesitter/nvim-treesitter-textobjects",
  after = "nvim-treesitter",
  requires = "nvim-treesitter/nvim-treesitter",
  config = function()
    require 'nvim-treesitter.configs'.setup {
      textobjects = {
        select = {
          enable = true,
          -- Automatically jump forward to textobj, similar to targets.vim
          lookahead = true,

          keymaps = {
            -- Default text objects:
            -- w: word
            -- W: WORD
            -- s: sentence
            -- p: paragraph
            -- t: tag
            -- quotes, brackets

            -- assignment / equation
            ["iq"] = "@assignment.inner",
            ["aq"] = "@assignment.outer",

            -- attribute
            ["ie"] = "@attribute.outer",
            ["ae"] = "@attribute.inner",

            -- if / loop / function / class
            ["ib"] = "@block.inner",
            ["ab"] = "@block.outer",

            -- call / method
            ["im"] = "@call.inner",
            ["am"] = "@call.outer",

            -- class
            ["ic"] = "@class.inner",
            ["ac"] = "@class.outer",

            -- comment
            ["io"] = "@comment.inner",
            ["ao"] = "@comment.outer",

            -- if
            ["ii"] = "@conditional.inner",
            ["ai"] = "@conditional.outer",

            -- function
            ["if"] = "@function.inner",
            ["af"] = "@function.outer",

            -- loop
            ["il"] = "@loop.inner",
            ["al"] = "@loop.outer",

            -- parameter value
            ["ia"] = "@parameter.inner",
            ["aa"] = "@parameter.outer",

            -- return value
            ["ir"] = "@return.inner",
            ["ar"] = "@return.outer",

            -- statement: this can select various things depending on
            -- the cursor position. Works for doc strings (triple quote
            -- strings) as well!!!
            ["ad"] = "@statement.outer",
          },
          -- You can choose the select mode (default is charwise 'v')
          --
          -- Can also be a function which gets passed a table with the keys
          -- * query_string: eg '@function.inner'
          -- * method: eg 'v' or 'o'
          -- and should return the mode ('v', 'V', or '<c-v>') or a table
          -- mapping query_strings to modes.
          selection_modes = {
            ["@parameter.outer"] = "v",
            ["@parameter.inner"] = "v",
            ["@function.outer"] = "V",
            ["@function.inner"] = "V",
            ["@class.outer"] = "V",
            ["@class.inner"] = "V",
            ["@comment.outer"] = "V",
            ["@comment.inner"] = "V",
            ["@loop.outer"] = "V",
            ["@loop.inner"] = "V",
            ["@conditional.outer"] = "V",
            ["@conditional.inner"] = "V",
          },
          include_surrounding_whitespace = false,
        },

        move = {
          enable = true,
          set_jumps = true, -- whether to set jumps in the jumplist
          goto_next_start = {
          },
          goto_next_end = {
          },
          goto_previous_start = {
          },
          goto_previous_end = {
          },
          -- Below will go to either the start or the end, whichever is closer.
          -- Use if you want more granular movements
          -- Make it even more gradual by adding multiple queries and regex.
          goto_next = {
          },
          goto_previous = {
          }
        },
      },
    }
  end
}
