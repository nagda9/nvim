return {
  "lewis6991/gitsigns.nvim",
  config = function()
    require("gitsigns").setup({
      sign_priority       = 1,
      signs               = {
        add = { text = "" },
        change = { text = "" },
        delete = { text = "" },
        topdelete = { text = "󱅁" },
        changedelete = { text = "󰍷" },
        untracked = { text = "" },
      },
      attach_to_untracked = true, -- show gitsingns on untracked files
    })
  end
}
