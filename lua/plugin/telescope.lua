-- Telescope (Fuzzy finder)
return {
  "nvim-telescope/telescope.nvim",
  lazy = false,
  dependencies = {
    'nvim-lua/plenary.nvim',

    -- Make telescope use fzf
    {
      "nvim-telescope/telescope-fzf-native.nvim",
      build = "make",
    },

    -- Live grep
    -- Note: requires ripgrep to be installed with apt
    "nvim-telescope/telescope-live-grep-args.nvim",
  },

  config = function()
    local telescope = require("telescope")

    telescope.setup({
      defaults = {
        file_ignore_patterns = { "venv/*", "build/*", ".git/", "assets/", "static/" }
      },

      pickers = {
        find_files = {
          hidden = true
        }
      }
    })

    telescope.load_extension("fzf")
    telescope.load_extension("live_grep_args")
  end
}
