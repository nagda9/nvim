return {
  "jpalardy/vim-slime",
  config = function()
    -- Configure vim-slime here if needed
    vim.g.slime_target = "tmux"
    vim.g.slime_bracketed_paste = 1
    vim.g.slime_default_config = {
      socket_name = "default",       -- the tmux socket name
      target_pane = "{last}",        -- target tmux pane; {last} sends to the last active pane
    }
    vim.g.slime_dont_ask_default = 1 -- prevents asking for target each time

    -- disables default bindings
    vim.g.slime_no_mappings = 1

    --send visual selection
    vim.keymap.set('x', '<leader>s', '<Plug>SlimeRegionSend', { silent = true })
    vim.keymap.set('n', '<leader>s', '<Plug>SlimeMotionSend', { silent = true })
    vim.keymap.set('n', '<leader>ss', '<Plug>SlimeParagraphSend', { silent = true })
  end
}
