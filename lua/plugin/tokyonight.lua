-- Colorscheme
return {
  "folke/tokyonight.nvim",
  lazy = false,
  priority = 1000,

  config = function()
    require("tokyonight").setup({
      style = "moon",
      on_colors = function(colors)
        colors.darkorange = "#8B4000"
      end,

      on_highlights = function(highlights, colors)
        -- Last search pattern highlighting (see 'hlsearch').
        highlights.Search = { bg = colors.bg_search, fg = colors.fg }
        -- Note: Also used for similar items that need to stand out.

        highlights.IncSearch = { bg = colors.darkorange, fg = colors.fg }
        -- Note: Also used for the text replaced with ":s///c"

        highlights.CurSearch = { link = "IncSearch" }
      end,
    })

    vim.cmd("colorscheme tokyonight")
  end
}
