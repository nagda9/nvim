local autocmd = vim.api.nvim_create_autocmd
local augroup = vim.api.nvim_create_augroup

-- Formatoptions: string (default "tcqj")
-- c: Auto-wrap comments using text width
-- j: Removes comment leaders when joining lines.
-- n:	Recognize numbered lists for formatting. Indent new lines to
-- match the number.
-- o: Automatically insert the current comment leader after hitting
-- o or O in Normal mode.
-- p:	Don't break lines that look like a numbered or bulleted list.
-- q: Allow formatting of comments with gq.
-- r: Automatically insert the current comment leader after hitting
-- Enter in textwidth in Insert mode unless explicitly typed.
-- t:	Auto-wrap text using textwidth.
-- There are other options, but I dont want to list them all here.
-- Use `:help fo-table` to check other options.
local formatoptions_group = augroup("user/formatting",
  { clear = true })
autocmd("FileType", {
  group = formatoptions_group,
  desc = "Prevent inserting a comment leader on new line",
  pattern = "*",
  command = "setlocal formatoptions=cjnpqrt"
})

-- Highlight yanking
local yank_group = augroup("user/yank", { clear = true })
autocmd("TextYankPost", {
  group = yank_group,
  desc = "Highlight yanking",
  callback = function()
    vim.highlight.on_yank()
  end
})

-- Luasnip stop snippets when you leave to normal mode
local snippets_group = augroup("user/snippets", { clear = true })
autocmd('ModeChanged', {
  group = snippets_group,
  desc = "Stop snippets when you leave to normal mode",
  pattern = '*',
  callback = function()
    if ((vim.v.event.old_mode == 's' and vim.v.event.new_mode == 'n')
          or vim.v.event.old_mode == 'i')
        and require('luasnip').session.current_nodes[vim.api.nvim_get_current_buf()]
        and not require('luasnip').session.jump_active
    then
      require('luasnip').unlink_current()
    end
  end
})


local fold_group = augroup("user/save_folds", { clear = true })
autocmd({ "BufWinLeave", "BufWritePre" }, {
  group = fold_group,
  desc = "Save folds when buffer is saved.",
  pattern = "*",
  command = "silent! mkview",
})
autocmd("BufWinEnter", {
  group = fold_group,
  desc = "Load folds when entering buffer",
  pattern = "*",
  command = "silent! loadview",
})
