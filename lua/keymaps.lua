local function map(modes, lhs, rhs, opts)
  for _, mode in ipairs(modes) do
    vim.api.nvim_set_keymap(mode, lhs, rhs, opts)
  end
end

local opts = { silent = true, noremap = true, nowait = true }

map({ "n" }, "<BS>", "<Nop>", opts)                                      -- same as h, move left
map({ "n" }, "<C-j>", ":cn<CR>", opts)                                   -- Next quicklist item
map({ "n" }, "<C-k>", ":cp<CR>", opts)                                   -- Prev quicklist item
map({ "n" }, "<C-n>", "<Nop>", opts)                                     -- same as j, move down
map({ "i" }, "<C-n>", "<Nop>", opts)                                     -- find next match for words that start with the keyword in front of the cursor
map({ "n" }, "<C-m>", "<Nop>", opts)                                     -- same as j, move down
map({ "n" }, "<C-p>", "<Nop>", opts)                                     -- same as k, move up
map({ "n", "v" }, "<C-s>", "<CMD>w<CR>", opts)                           -- save buffer
map({ "n" }, "<C-z>", "<Nop>", opts)                                     -- suspend program
map({ "n" }, "<C-S-n>", "<Nop>", opts)                                   -- same as j, move down a lot
map({ "i" }, "<C-S-n>", "<Nop>", opts)                                   -- find next match for words that start with the keyword in front of the cursor
map({ "n" }, "<C-S-m>", "<Nop>", opts)                                   -- same as j, move down a lot
map({ "n" }, "<C-End>", "<Nop>", opts)                                   -- same as G, jump to last line
map({ "n" }, "<C-Home>", "<Nop>", opts)                                  -- same as gg, jump to first line
map({ "n" }, "<Esc>", "<CMD>nohlsearch<CR>", opts)                       -- disappear highlights
map({ "i" }, '<C-c>', '<Esc>', opts)                                     -- do autocommand InsertLeave
map({ "n", "i" }, "<F1>", "<Nop>", opts)                                 -- open vim help
map({ "n" }, "<Leader>bd", "<CMD>Bdelete<CR>", opts)                     -- delete buffer
map({ "n" }, "<Leader>ba", "<CMD>wa | silent! %bd | e# | bd#<CR>", opts) -- delete buffer
map({ "n" }, "<Leader>e", "<CMD>Oil<CR>", opts)                          -- explorer
map({ "n" }, "<Leader>ff", "<CMD>Telescope find_files<CR>", opts)        -- telescope find files
map({ "n" }, "<Leader>fg", "<CMD>Telescope live_grep_args<CR>", opts)    -- telescope find argument
map({ "n" }, "<Leader>uu", "<CMD>UndotreeToggle<CR>", opts)              -- toggle undotree
map({ "n" }, "<M-Down>", ":m .+1<CR>==", opts)                           -- move line down
map({ "v" }, "<M-Down>", ":m '>+1<CR>gv=gv", opts)                       -- move block up
map({ "n" }, "<M-Up>", ":m .-2<CR>==", opts)                             -- move line up
map({ "v" }, "<M-Up>", ":m '<-2<CR>gv=gv", opts)                         -- move block down
map({ "n" }, "<NL>", "<Nop>", opts)                                      -- same as j, move down
map({ "n" }, "<Space><Space>", "<Nop>", opts)                            -- same as l, move right
map({ "n" }, "H", "<CMD>bprevious<CR>", opts)                            -- prev buffer
map({ "n" }, "L", "<CMD>bnext<CR>", opts)                                -- next buffer
map({ "n" }, "oo", "o<Esc>", opts)                                       -- insert line after
map({ "n" }, "OO", "O<Esc>", opts)                                       -- insert line before
map({ "v" }, "p", '\"_dP', opts)                                         -- paste over text and not storing the deleted text
map({ "v" }, "<S-Tab>", "<gv^", opts)                                    -- indent line backward
map({ "v" }, "<Tab>", ">gv^", opts)                                      -- indent line foreward
map({ "n" }, "ZZ", "<Nop>", opts)                                        -- suspend program
map({ "n" }, "ZQ", "<Nop>", opts)                                        -- close window without writing
